package com.vaadin.tutorial.crm.backend.service;

import com.github.javafaker.Faker;
import com.github.javafaker.service.FakeValuesService;
import com.github.javafaker.service.RandomService;
import com.vaadin.tutorial.crm.backend.entity.Company;
import com.vaadin.tutorial.crm.backend.entity.Contact;
import com.vaadin.tutorial.crm.backend.repository.CompanyRepository;
import com.vaadin.tutorial.crm.backend.repository.ContactRepository;
import org.hibernate.hql.spi.id.local.LocalTemporaryTableBulkIdStrategy;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

@Service
public class ContactService {
    private static final Logger LOGGER = Logger.getLogger(ContactService.class
            .getName());
    private ContactRepository contactRepository;
    private CompanyRepository companyRepository;

    public ContactService(ContactRepository contactRepository,
                          CompanyRepository companyRepository) {
        this.contactRepository = contactRepository;
        this.companyRepository = companyRepository;
    }

    public List<Contact> findAll() {
        return contactRepository.findAll();
    }

    public List<Contact> findAll(String stringfilter) {
        if (stringfilter == null || stringfilter.isEmpty())
            return contactRepository.findAll();
        else
            return contactRepository.search(stringfilter);
    }

    public long count() {
        return contactRepository.count();
    }

    public void delete(Contact contact) {
        contactRepository.delete(contact);
    }

    public void save(Contact contact) {
        if (contact == null) {
            LOGGER.log(Level.SEVERE, "Contact is null. Are you sure you have connected your form to the application?");
            return;
        }
        contactRepository.save(contact);
    }

    @PostConstruct
    public void populateTestData() {

        final Faker f = new Faker();

        if (companyRepository.count() == 0) {
            companyRepository.saveAll(
                    Stream.of(f.company().name(),
                            f.company().name(),
                            f.company().name(),
                            f.company().name(),
                            f.company().name(),
                            f.company().name())
                            .map(Company::new)
                            .collect(toList()));
        }


        if (contactRepository.count() == 0) {
            Random r = new Random(0);
            List<Company> companies = companyRepository.findAll();



            final List<Contact> contacts =
                    Stream.generate(() -> {
                                Contact c = new Contact();
                                c.setFirstName(f.name().firstName());
                                c.setLastName(f.name().lastName());
                                c.setCompany(companies.get(r.nextInt(companies.size())));
                                c.setStatus(Contact.Status.values()[r.nextInt(Contact
                                        .Status.values().length)]);
                                c.setEmail(f.bothify("????##@gmail.com"));
                                return c;
                            }
                    )

                            .limit(1000)
                            .collect(toList());

            contactRepository.saveAll(contacts);

        }
    }
}